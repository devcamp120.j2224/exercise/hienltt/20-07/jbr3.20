package com.devcamp.s10.splitstring_api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitControl {
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> getSplit() {

        String string = "Split String to ArrayList RestAPI";
        String[] split = string.split("\\s");

        ArrayList<String> arrList = new ArrayList<>();
        for (String word : split) {
            arrList.add(word);
        }
        return arrList;
    }
}
